from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:

    output = list(
        input_
    )  # Convertit l'input en une liste pour représenter la bande de la machine de Turing
    current_state = machine["start state"]  # État courant initialisé à l'état de départ
    current_position = 0  # Position courante de la tête de lecture/écriture

    history_exe = []  # Liste pour stocker l'exécution des états
    history_exe.append(current_state)  # Ajoute l'état de départ à l'ordre d'exécution

    while steps is None or steps > 0:

        if current_state in machine["final states"]:
            return (
                "".join(output),
                history_exe,
                True,
            )  # Si l'état courant est un état final, retourne le résultat

        if current_position < 0:
            output.insert(
                0, machine["blank"]
            )  # Insère Blank si la tête de lecture/écriture sort de la bande à gauche
            current_position = 0
        elif current_position >= len(output):
            output.append(
                machine["blank"]
            )  # Ajoute Blank si la tête de lecture/écriture sort de la bande à droite

        current_symbol = output[current_position]
        transition = machine["table"][current_state].get(current_symbol)

        if transition is None:
            return (
                "".join(output),
                history_exe,
                False,
            )  # Si aucune transition n'est définie, retourne le résultat avec False

        output[current_position] = transition.get(
            "write", current_symbol
        )  # Écrit le nouveau symbole sur la bande

        if transition.get("L"):
            current_position -= 1  # Déplace la tête de lecture/écriture vers la gauche
        elif transition.get("R"):
            current_position += 1  # Déplace la tête de lecture/écriture vers la droite

        current_state = transition.get("L") or transition.get(
            "R"
        )  # Met à jour l'état courant

        history_exe.append(current_state)  # Ajoute l'état courant à l'ordre d'exécution

        if steps is not None:
            steps -= 1

    return (
        "".join(output),
        history_exe,
        False,
    )  # Si le nombre maximum d'éoutputs est atteint, retourne le résultat avec False
